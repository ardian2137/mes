package Growth;

public class Settings {
	
	public static boolean isRunning = false;
	public static int boundaryCondition = 0;
	public static int delay = 20;
	public static int pixelSize = 5;
	
	public static int neighborhoodType = 0;
	public static int startingPoints = 8;
	public static int placingType = 1;
	
	public static boolean isRecrystalizationActive = false;
	public static boolean isMonteCarloActive = false; 
	
}
