package Growth;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.swing.JPanel;

public class Board extends JPanel implements Runnable {

	private Random generator = new Random();
	private int pixelSize = Settings.pixelSize;
	private int width = 1200;
	private int height = 600;
	private int tabSizeX = this.width / this.pixelSize;
	private int tabSizeY = this.height / this.pixelSize;
	private Pixel[][] Pixels = new Pixel[this.tabSizeX][this.tabSizeY];
	private int minimunRecrystalizationRadius = 100; 
	
	private int actualItaration = 0; 
	private Map<Integer, int[]> pattern;
	private List<Pixel> colors = new ArrayList<Pixel>();

	public Board() {

		pattern = new HashMap<Integer, int[]>();

		pattern.put(0, new int[] { 1, 1, 1, 1, 1, 1, 1, 1 }); // Moore
		pattern.put(1, new int[] { 0, 1, 0, 1, 0 ,1, 0, 1 }); // von Nauman
		pattern.put(2, new int[] { 1, 1, 0, 1, 1, 1, 0, 1 }); // Hexagonal left
		pattern.put(3, new int[] { 0, 1, 1, 1, 0, 1, 1, 1 }); // Hexagonal right
		pattern.put(4, new int[] { }); // Hexagonal random
		pattern.put(5, new int[] { }); // Pentagonal random
		
		for (int i = 0; i < this.tabSizeX; i++) {
			for (int j = 0; j < this.tabSizeY; j++) {
				this.Pixels[i][j] = new Pixel();
			}
		}

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int x = arg0.getX();
				int y = arg0.getY();
				Board.this.clickPixel(x, y);
				Board.this.refresh();
			}
		});

	}
    
	public void run() {
		while (true) {
			if (Settings.isRunning) {
				this.reverse(); 
				this.refresh();
			} else { actualItaration = 0; }
			try {
				Thread.sleep(Settings.delay);
			} catch (Exception ex) { }
		}
	}
	
	private void reverse() {
		for (int i = 0; i < this.tabSizeX; i++) {
			for (int j = 0; j < this.tabSizeY; j++) {
				this.Pixels[i][j].prev = this.Pixels[i][j].act;
				this.Pixels[i][j].rek_prev = this.Pixels[i][j].rek;
			}
		}
	}
	
	public void refresh() {
		
		int freePixels = this.basicIteration();
		
		if (freePixels==0 && Settings.isRecrystalizationActive) {
			++actualItaration;
			this.recrystalizationIteration();
		}
		
		else if (Settings.isMonteCarloActive) {
			if (actualItaration<2);
			++actualItaration;
			this.monteCarloIteration();
		}
		repaint();
	}
	
	private int basicIteration() {
		if (!Settings.isRunning) return -1;
		int x = 0;
		Pixel tmp;
		
			for (int i = 0; i <= this.tabSizeX - 1; i++) {
				for (int j = 0; j <= this.tabSizeY - 1; j++) {
					if (this.Pixels[i][j].act == 1)
						continue;
					++x;
					tmp = this.findColoredPixel(i, j, "act");
					if (tmp.act < 0 && tmp.prev < 0)
						continue;
					this.Pixels[i][j].act = tmp.act;
					this.Pixels[i][j].recolor(tmp);
					
				}
			}
		return x;
	}
	
	private void monteCarloIteration() {

		List<Pixel> chosenPixels = new ArrayList<Pixel>();
		Pixel newPixel = new Pixel();
		
		for (int i = 0; i < this.tabSizeX; i++) {
			for (int j = 0; j < this.tabSizeY; j++) {
				chosenPixels.clear();
				int PixelEnergy= this.countEnergy(i,j);
				int newPixelEnergy;
				do {
					do {
						newPixel = colors.get(generator.nextInt(colors.size()));
					} while(chosenPixels.contains(newPixel));
					chosenPixels.add(newPixel);
					
					this.Pixels[i][j].recolor(newPixel);

					newPixelEnergy = this.countEnergy(i,j);
				} while (newPixelEnergy>PixelEnergy && chosenPixels.size()<colors.size());
				
			}
		}	
	}
		
	private Pixel findColoredPixel(int x, int y, String type) {
		int[] neighbors = { 0, 0, 0, 0, 0, 0, 0, 0 };
		int[] temporaryPattern = { 0, 0, 0, 0, 0, 0, 0, 0 };

		/**
		 * 0 - lewa gorna; 1 - centralna gorna; 2 - prawa gorna; 3 - prawa
		 * srodkowa 4 - prawa dolna; 5 - centralna dolna; 6 - lewa dolna; 7 -
		 * lewa srodkowa
		 */
		int leftX = x - 1;
		int rightX = x + 1;
		int topY = y - 1;
		int bottomY = y + 1;

		setBoundaryCondition(leftX, rightX,topY,bottomY);
		
		neighbors = setNeighbors(neighbors,x,y,leftX,rightX,topY,bottomY,type );

		temporaryPattern = chooseNeighborhoodType();
		
		return choosePixelToColorFrom(neighbors,temporaryPattern,x,y,leftX,rightX,topY,bottomY );

	}
	
	public void setBoundaryCondition(int leftX,int rightX,int topY,int bottomY){
		if (Settings.boundaryCondition == 0) { 
			if (leftX < 0)
				leftX = this.tabSizeX - 1;
			if (rightX > this.tabSizeX - 1)
				rightX = 0;

			if (topY < 0)
				topY = this.tabSizeY - 1;
			else if (bottomY > this.tabSizeY - 1)
				bottomY = 0;
		}
	}
	public int[] setNeighbors(int neighbors[],int x, int y,int leftX,int rightX,int topY,int bottomY,String type ){
		if (type=="act") {
			if (leftX >= 0 && topY >= 0)
				neighbors[0] = this.Pixels[leftX][topY].prev;
			if (topY >= 0)
				neighbors[1] = this.Pixels[x][topY].prev;
			if (topY >= 0 && rightX <= this.tabSizeX - 1)
				neighbors[2] = this.Pixels[rightX][topY].prev;
			if (rightX <= this.tabSizeX - 1)
				neighbors[3] = this.Pixels[rightX][y].prev;
			if (bottomY <= this.tabSizeY - 1 && rightX <= this.tabSizeX - 1)
				neighbors[4] = this.Pixels[rightX][bottomY].prev;
			if (bottomY <= this.tabSizeY - 1)
				neighbors[5] = this.Pixels[x][bottomY].prev;
			if (leftX >= 0 && bottomY <= this.tabSizeY - 1)
				neighbors[6] = this.Pixels[leftX][bottomY].prev;
			if (leftX >= 0)
				neighbors[7] = this.Pixels[leftX][y].prev;
			} else if (type=="rek") {
				if (leftX >= 0 && topY >= 0)
					neighbors[0] = this.Pixels[leftX][topY].rek_prev;
				if (topY >= 0)
					neighbors[1] = this.Pixels[x][topY].rek_prev;
				if (topY >= 0 && rightX <= this.tabSizeX - 1)
					neighbors[2] = this.Pixels[rightX][topY].rek_prev;
				if (rightX <= this.tabSizeX - 1)
					neighbors[3] = this.Pixels[rightX][y].rek_prev;
				if (bottomY <= this.tabSizeY - 1 && rightX <= this.tabSizeX - 1)
					neighbors[4] = this.Pixels[rightX][bottomY].rek_prev;
				if (bottomY <= this.tabSizeY - 1)
					neighbors[5] = this.Pixels[x][bottomY].rek_prev;
				if (leftX >= 0 && bottomY <= this.tabSizeY - 1)
					neighbors[6] = this.Pixels[leftX][bottomY].rek_prev;
				if (leftX >= 0)
					neighbors[7] = this.Pixels[leftX][y].rek_prev;
			}
			
			return neighbors;
	}
	
	public int[] chooseNeighborhoodType(){
		switch (Settings.neighborhoodType) {
		case 4:
			return pattern.get(generator.nextInt(2)+1) ;
		case 5:
			return pattern.get(generator.nextInt(2));
		default:
			return pattern.get(Settings.neighborhoodType);
		}
	}
	
	public Pixel choosePixelToColorFrom(int neighbors[],int temporaryPattern[],int x, int y,int leftX,int rightX,int topY,int bottomY ){
		Pixel newPixel = new Pixel();
		newPixel.setPixel(-1);
		
		if (neighbors[0] == 1 && temporaryPattern[4] == 1)
			return this.Pixels[leftX][topY];
		else if (neighbors[1] == 1 && temporaryPattern[5] == 1)
			return this.Pixels[x][topY];
		else if (neighbors[2] == 1 && temporaryPattern[6] == 1)
			return this.Pixels[rightX][topY];
		else if (neighbors[3] == 1 && temporaryPattern[7] == 1)
			return this.Pixels[rightX][y];
		else if (neighbors[4] == 1 && temporaryPattern[0] == 1)
			return this.Pixels[rightX][bottomY];
		else if (neighbors[5] == 1 && temporaryPattern[1] == 1)
			return this.Pixels[x][bottomY];
		else if (neighbors[6] == 1 && temporaryPattern[2] == 1)
			return this.Pixels[leftX][bottomY];
		else if (neighbors[7] == 1 && temporaryPattern[3] == 1)
			return this.Pixels[leftX][y];
		
		return newPixel;
	}
		
	private int countEnergy(int x, int y) {
		Pixel[] neighbors = new Pixel[8];
		
		/**
		 * 0 - lewa gorna; 1 - centralna gorna; 2 - prawa gorna; 3 - prawa
		 * srodkowa 4 - prawa dolna; 5 - centralna dolna; 6 - lewa dolna; 7 -
		 * lewa srodkowa
		 */
		int leftX = x - 1;
		int rightX = x + 1;
		int topY = y - 1;
		int bottomY = y + 1;

		if (Settings.boundaryCondition == 0) { 
			if (leftX < 0)
				leftX = this.tabSizeX - 1;
			if (rightX > this.tabSizeX - 1)
				rightX = 0;

			if (topY < 0)
				topY = this.tabSizeY - 1;
			else if (bottomY > this.tabSizeY - 1)
				bottomY = 0;
		}

		neighbors = setNeighbors(neighbors,x,y,leftX,rightX,topY,bottomY);
				
		int pixelEnergy = 0;;
		for (Pixel i : neighbors) {
			if (!this.Pixels[x][y].colorMatch(i)) ++ pixelEnergy;
		}
		return pixelEnergy;
	}
	
	public Pixel[] setNeighbors(Pixel neighbors[],int x, int y,int leftX,int rightX,int topY,int bottomY){
			if (leftX >= 0 && topY >= 0)
				neighbors[0] = this.Pixels[leftX][topY];
			if (topY >= 0)
				neighbors[1] = this.Pixels[x][topY];
			if (topY >= 0 && rightX <= this.tabSizeX - 1)
				neighbors[2] = this.Pixels[rightX][topY];
			if (rightX <= this.tabSizeX - 1)
				neighbors[3] = this.Pixels[rightX][y];
			if (bottomY <= this.tabSizeY - 1 && rightX <= this.tabSizeX - 1)
				neighbors[4] = this.Pixels[rightX][bottomY];
			if (bottomY <= this.tabSizeY - 1)
				neighbors[5] = this.Pixels[x][bottomY];
			if (leftX >= 0 && bottomY <= this.tabSizeY - 1)
				neighbors[6] = this.Pixels[leftX][bottomY];
			if (leftX >= 0)
				neighbors[7] = this.Pixels[leftX][y];
			return neighbors;
			}
	
	private void recrystalizationIteration() {
		double dyslocationDensity = this.countDyslocationDensity(actualItaration) - this.countDyslocationDensity(actualItaration-1);  // roznica dyslokacji
		double dyslocationDensityOnPixel  = dyslocationDensity /(this.tabSizeX*this.tabSizeY); 
		double criticalDyslocationDensity = 4215840144233.42/(this.tabSizeX*this.tabSizeY);
		double temporaryDyslocationDensity;
		Pixel newPixel;
			
		for (int i = 0; i <= this.tabSizeX - 1; i++) {
			for (int j = 0; j <= this.tabSizeY - 1; j++) {

				if (this.Pixels[i][j].rek==0) {
					if (this.onBorder(i, j)) {
						temporaryDyslocationDensity = this.randomAverageDensity(50, 150, dyslocationDensityOnPixel);
					} else {
						temporaryDyslocationDensity = this.randomAverageDensity(0, 300, dyslocationDensityOnPixel);
					}
				
					this.Pixels[i][j].dyslocationDensity += temporaryDyslocationDensity;
				}
								
				if (this.Pixels[i][j].rek==1) continue; 
				
				if (this.Pixels[i][j].dyslocationDensity > criticalDyslocationDensity && this.isRecristalizationPossible(i, j)) {
					this.Pixels[i][j].setRecristalizationStatus(1);
				}
			}
		}
		
		for (int i = 0; i <= this.tabSizeX - 1; i++) {
			for (int j = 0; j <= this.tabSizeY - 1; j++) {
				if (this.Pixels[i][j].act==0 || this.Pixels[i][j].rek==1) continue; 

				newPixel = this.findColoredPixel(i, j, "rek"); 
				
				if (newPixel.act < 0 ) 	continue;

				if (newPixel.rek==1 && this.Pixels[i][j].rek==0) { 
					this.Pixels[i][j].dyslocationDensity = 0.0;
					this.Pixels[i][j].rek = newPixel.rek;
					this.Pixels[i][j].recolorRecristalization(newPixel);
				}
				
			}
		}
	}

	private Double countDyslocationDensity(int actualItaration) {
		if (actualItaration<0) return 0.0;
		Double dyslocationDensity;
		
		double A = 86710969050178.5;
		double B = 9.41268203527779;
		double t = (double)actualItaration * (double)(Settings.delay)/1000.0;
		
		dyslocationDensity = A/B+(1-A/B)*Math.exp(-B*t);
		return dyslocationDensity;
	}
	
	private boolean onBorder(int x, int y) {
		boolean[] neighbors = new boolean[8];

		int leftX = x - 1;
		int rightX = x + 1;
		int topY = y - 1;
		int bottomY = y + 1;

		setBoundaryCondition(leftX, rightX,topY,bottomY);

		neighbors = setNeighbors(neighbors,x,y,leftX,rightX,topY,bottomY);
		
		for(boolean s : neighbors) {
			if (!s) return true;
		}
		return false;

	}
	
	public boolean[] setNeighbors(boolean neighbors[],int x, int y,int leftX,int rightX,int topY,int bottomY){
		if (leftX >= 0 && topY >= 0)
			neighbors[0] = this.Pixels[leftX][topY].colorMatch(this.Pixels[x][y]);
		if (topY >= 0)
			neighbors[1] = this.Pixels[x][topY].colorMatch(this.Pixels[x][y]);
		if (topY >= 0 && rightX <= this.tabSizeX - 1)
			neighbors[2] = this.Pixels[rightX][topY].colorMatch(this.Pixels[x][y]);
		if (rightX <= this.tabSizeX - 1)
			neighbors[3] = this.Pixels[rightX][y].colorMatch(this.Pixels[x][y]);
		if (bottomY <= this.tabSizeY - 1 && rightX <= this.tabSizeX - 1)
			neighbors[4] = this.Pixels[rightX][bottomY].colorMatch(this.Pixels[x][y]);
		if (bottomY <= this.tabSizeY - 1)
			neighbors[5] = this.Pixels[x][bottomY].colorMatch(this.Pixels[x][y]);
		if (leftX >= 0 && bottomY <= this.tabSizeY - 1)
			neighbors[6] = this.Pixels[leftX][bottomY].colorMatch(this.Pixels[x][y]);
		if (leftX >= 0)
			neighbors[7] = this.Pixels[leftX][y].colorMatch(this.Pixels[x][y]);
		
		return neighbors;
		}
	
	private double randomAverageDensity(int start, int end, double dyslocationDensityOnPixel) {
		int range = end-start;
		return (double)((generator.nextInt(range)+start)/range)*dyslocationDensityOnPixel;
	}
	
	private boolean isRecristalizationPossible(int x, int y) {
		for (int i=1;i<this.minimunRecrystalizationRadius;i++) {
			for (int j=1;j<this.minimunRecrystalizationRadius;j++) {
				if (y-j>=0 && x-i>=0) if (this.Pixels[x-i][y-j].rek==1) return false;
				if (y+j<this.tabSizeY && x+i<this.tabSizeX) if (this.Pixels[x+i][y+j].rek==1) return false;
			}
		}
		return true;
	}
	
	public void randomPixel() {
		int x = generator.nextInt(this.width);
		int y = generator.nextInt(this.height);
		this.clickPixel(x, y);
	}
	
	private void clickPixel(int x, int y) {
		int pixel_x = x / this.pixelSize;
		int pixel_y = y / this.pixelSize;

		while (pixel_x < 1)
			pixel_x++;
		while (pixel_x >= this.tabSizeX - 1)
			pixel_x--;
		while (pixel_y < 1)
			pixel_y++;
		while (pixel_y >= this.tabSizeY - 1)
			pixel_y--;

		if (pixel_x >= this.tabSizeX || pixel_y >= this.tabSizeY)
			return;

		if (this.Pixels[pixel_x][pixel_y].act == 0) {
			this.Pixels[pixel_x][pixel_y] = new Pixel();
			this.Pixels[pixel_x][pixel_y].setRandomColor(1);
		}

		this.reverse();
	}

	public void positionPlacement() {
		Settings.isMonteCarloActive = false;

		switch (Settings.placingType) {
		case 1:
			equalPlacement();
			break;
		case 2: 
			radiusRandomPlacement();
			break; 

		case 3: 
			pureRandomPlacement();
			break;

		case 4: 
			monteCarloPlacement();
			break;
		}
	}
	
	public void equalPlacement(){
		double px,py;
		
		py = Math.sqrt(this.tabSizeY*Settings.startingPoints/this.tabSizeX);
		px = ((double)Settings.startingPoints)/py;
		int xSize = (int)px;
		int ySize = (int)py;
		int xWidth = this.tabSizeX/xSize;
		int yHeight = this.tabSizeY/ySize;

		for (int i=0;i<xSize;i++) {
			for(int j=0;j<ySize;j++) {
				int tX = (i*2+1)*xWidth/2, tY = (j*2+1)*yHeight/2;
				
				do {
					this.Pixels[tX][tY].setRandomColor(1);
				} while ( this.colorExists(this.Pixels[tX][tY],tX,tY) );

				colors.add(new Pixel(this.Pixels[tX][tY]));
			}
		}
	}
	
	public void radiusRandomPlacement(){
		
		int xCenter = this.tabSizeX / 2;
		int yCenter = this.tabSizeY / 2;
		int PRx,PRy;

		for (int i = 0; i < 360; i = i + (360 / Settings.startingPoints)) {
			PRx = generator.nextInt(this.tabSizeX / 2);
			PRy = generator.nextInt(this.tabSizeY / 2);

			int x = (int) (PRx * Math.cos(i * Math.PI / 180));
			x = x + xCenter;
			int y = (int) (PRy * Math.sin(i * Math.PI / 180));
			y = y + yCenter;

			while (x < 0)
				x++;
			while (y < 0)
				y++;
			while (x >= this.tabSizeX)
				x--;
			while (y >= this.tabSizeY)
				y--;

			do {
				this.Pixels[x][y].setRandomColor(1);
			} while (this.colorExists(this.Pixels[x][y],x,y));

			colors.add(new Pixel(this.Pixels[x][y]));
		}
	}
	
	public void pureRandomPlacement(){
		for (int i=0;i<Settings.startingPoints;i++) {
			int x = generator.nextInt(this.tabSizeX);
			int y = generator.nextInt(this.tabSizeY);
			this.Pixels[x][y].setRandomColor(1);
			while (this.colorExists(this.Pixels[x][y],x,y)) this.Pixels[x][y].setRandomColor(1); // losuj nowe az do uzyskania unikatu

			colors.add(new Pixel(this.Pixels[x][y]));
		}
	}
	
	public void monteCarloPlacement(){
	Settings.isMonteCarloActive = true;
	for (int i=0;i<Settings.startingPoints;i++) {
		Pixel newPixel = new Pixel(); newPixel.setRandomColor(0);
		while (this.colorExists(newPixel,-1,-1)) newPixel.setRandomColor(0);
		colors.add(new Pixel(newPixel));
	}
	
	for (int i = 0; i < this.tabSizeX; i++) {
		for (int j = 0; j < this.tabSizeY; j++) {
			this.Pixels[i][j].setPixel(1);
			this.Pixels[i][j].recolor( colors.get( generator.nextInt( colors.size() ) ) );
		}
	}
}
	
	private boolean colorExists(Pixel p, int x, int y) {
		for (int i = 0; i < this.tabSizeX; i++) {
			for (int j = 0; j < this.tabSizeY; j++) {
				if (x==i && y==j && x>=0 && y>=0) continue;
				if (this.Pixels[i][j].colorMatch(p)) return true;
			}//j
		}//i
		return false;
	}

	@Override
	public void paintComponent(Graphics g) {

		for (int x = 0; x < this.tabSizeX; x++) {
			for (int y = 0; y < this.tabSizeY; y++) {

				if (this.Pixels[x][y].RR>0 && this.Pixels[x][y].GG>0 && this.Pixels[x][y].BB>0) 
					g.setColor(new Color(this.Pixels[x][y].RR, this.Pixels[x][y].GG,
							this.Pixels[x][y].BB));
				else
				g.setColor(new Color(this.Pixels[x][y].R, this.Pixels[x][y].G,
						this.Pixels[x][y].B));

				g.fillRect(x * this.pixelSize, y * this.pixelSize,this.pixelSize, this.pixelSize );
			}
		}
	}

	public void clean() {
		this.actualItaration = 0; 
		
		for (int i = 0; i < this.tabSizeX; i++) {
			for (int j = 0; j < this.tabSizeY; j++) {
				this.Pixels[i][j].setPixel(0);
			}
		}
	}
}