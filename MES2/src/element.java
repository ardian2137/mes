
public class element {
	
	public node n1,n2;
	public double k,deltaR,C,ro,deltaTau;
	public double[][] K = new double[2][2];//lokalna macierz K
	public double[] F = new double[2]; //lokalna macierz F
	
	private double W(int id) {
		return 1.0;
	}
	
	private double E(int id) { //zahardkodowane dla 2 wez��w interpolacyjnych 
		if(id==1) {
			return -0.5773502692;
		}
		else if(id==2)
			return 0.5773502692;
		else
			return 0.0;
	}
	
	private double N1(int id) {	
			return (1-E(id))/2;
	}
	
	private double N2(int id) {
			return (1+E(id))/2;
	}
	
	private double R(int id){ 
		return ((N1(id)*n1.x)+(N2(id)*n2.x));
		}
	
	private double Temp(int id) {
		return ((N1(id)*n1.t)+(N2(id)*n2.t));
	}
	
	public void F() {
		for(int id=1;id<3;id++){
			F[0]-=(C*ro*deltaR*R(id)*W(id)*N1(id)*Temp(id))/deltaTau;
			F[1]-=(C*ro*deltaR*R(id)*W(id)*N2(id)*Temp(id))/deltaTau;
		}
	}
	public void K() {
		for(int id=1;id<3;id++){
			K[0][0] += (k*R(id)*W(id))/deltaR+(C*ro*deltaR*R(id)*N1(id)*N1(id))/deltaTau;
			K[0][1] += (-1)*(k*R(id)*W(id))/deltaR+(C*ro*deltaR*R(id)*N1(id)*N2(id))/deltaTau;
			K[1][0] = K[0][1];
			K[1][1] += (k*R(id)*W(id))/deltaR+(C*ro*deltaR*R(id)*N2(id)*N2(id))/deltaTau;
		}
	}
	
	public void F0() {
			F[0] = 0.0;
			F[1] = 0.0;
	}
	
	element(node node1, node node2,double H, double c, double RO, double DeltaTau) {
			C = c;
			ro = RO;
			deltaTau = DeltaTau;
			k = H;
			n1 = node1;
			n2 = node2;
			deltaR = n2.x-n1.x;
			K[0][0] =0.0;
			K[0][1] =0.0;
			K[1][0] =0.0;
			K[1][1] =0.0;
			F0();	
		}
}
