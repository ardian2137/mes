import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

public class Main {
	
	static final double EPSILON = 1e-10;
	public static double[] lsolve(double[][] A, double[] b) { // funkcja licz�ca wyniki z [K]{t}+{F}= 0, nie mojego autorstwa
        int N  = b.length;

        for (int p = 0; p < N; p++) {

            // find pivot row and swap
            int max = p;
            for (int i = p + 1; i < N; i++) {
                if (Math.abs(A[i][p]) > Math.abs(A[max][p])) {
                    max = i;
                }
            }
            double[] temp = A[p]; A[p] = A[max]; A[max] = temp;
            double   t    = b[p]; b[p] = b[max]; b[max] = t;

            // singular or nearly singular
            if (Math.abs(A[p][p]) <= EPSILON) {
                throw new RuntimeException("Matrix is singular or nearly singular");
            }

            // pivot within A and b
            for (int i = p + 1; i < N; i++) {
                double alpha = A[i][p] / A[p][p];
                b[i] -= alpha * b[p];
                for (int j = p; j < N; j++) {
                    A[i][j] -= alpha * A[p][j];
                }
            }
        }

        // back substitution
        double[] x = new double[N];
        for (int i = N - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < N; j++) {
                sum += A[i][j] * x[j];
            }
            x[i] = (b[i] - sum) / A[i][i];
        }
        return x;
    }

	public static void main(String[] args) {
		
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		int nodeSize=0,elementSize=0,numPoints=2,type,ten,next,id1,id2;
		double rMax=0.0,alfa=0.0,q=0.0,tBegin=0.0,t8=0.0,s=0.0,x,S;//pocz�tkowo niekt�re zmienna ustawiam na 0 niemniej potem wczytam ich warto�ci z pliku
		double k=0.0,rMin=0.0,tauMax=0.0,ro=0.0,C=0.0,deltaTau=0.0,deltaR=0.0,tau1=0.0,tau2=0.0,temp=0.0,tau=0.0;;	
		Network network = new Network();
		File loadPath=new File ("files/test.txt");//lokalizacja pliku do wczytania
		
		//Odczyt z pliku
		try {
			Scanner loadScanner = new Scanner(loadPath);
			while(loadScanner.hasNext()) {
				nodeSize = loadScanner.nextInt();
				elementSize = loadScanner.nextInt();
				network = new Network(nodeSize,elementSize);
				rMin = loadScanner.nextDouble();
				rMax = loadScanner.nextDouble();
				deltaR =(rMax-rMin)/elementSize;
				alfa = loadScanner.nextDouble();
				k = loadScanner.nextDouble();
				tauMax = loadScanner.nextDouble();
				deltaTau =  loadScanner.nextDouble();
				tBegin = loadScanner.nextDouble();
				t8 = loadScanner.nextDouble();
				ro = loadScanner.nextDouble();
				C = loadScanner.nextDouble();						
				}
			
		} catch (FileNotFoundException e) {
			System.out.println("Cos, cos sie popsulo");
		}
		
		for(int i=0;i<nodeSize;i++) {
			temp = tBegin;
			ten = i;
			next = i+1;
			x = i*deltaR;
			network.nodes[ten] = new node(x,ten,next,temp);
		}
		double[] results=new double[nodeSize];
		for(int i=0;i<elementSize;i++) {
			id1 = i;
			id2 = i+1;
			network.elements[i] = new element(network.nodes[id1],network.nodes[id2],k,C,ro,deltaTau);
		}
		
		double [] finalF =  new double[nodeSize]; //obliczanie wektora F
		double [][] finalK =  new double[nodeSize][nodeSize];//inicjalizacja macierzy globalnej H 
		for(int i=0;i<nodeSize;i++) {
			finalF[i] = 0;
			network.nodes[i].t = tBegin;
			for(int j=0;j<nodeSize;j++) {
				finalK[i][j] = 0.0;
			}
		}
		
		for(int i=0;i<elementSize;i++) {
			network.elements[i].K();
		}
		
		while(tau<tauMax) {
			tau+=deltaTau;
			for(int i=0;i<elementSize;i++) {
				network.elements[i].F();
			}
			
			for(int i=0;i<elementSize;i++) { // ��czenie macierzy lokalnych H w globaln�
				finalF[network.elements[i].n1.ten]+=network.elements[i].F[0];
				finalF[network.elements[i].n1.next]+=network.elements[i].F[1];
				finalK[network.elements[i].n1.ten][network.elements[i].n1.ten] += network.elements[i].K[0][0];
				finalK[network.elements[i].n1.ten][network.elements[i].n1.next] += network.elements[i].K[0][1];
				finalK[network.elements[i].n1.next][network.elements[i].n1.ten] += network.elements[i].K[1][0];
				finalK[network.elements[i].n1.next][network.elements[i].n1.next] +=network.elements[i].K[1][1];
			}
			
			//dodawnaie warunk�w brzegowych
			finalK[elementSize][elementSize] += alfa*rMax*4;
			finalF[elementSize] -= alfa*rMax*t8*4;
			
			System.out.println("Wyniki po czasie:"+tau);
			//pomocnicze wypisywanie komponent�w aby sprawdzi� czy s� dobrze liczone
			System.out.println("Polaczona Macierz K:"); 
			for(int i=0;i<nodeSize;i++) {
				for(int j=0;j<nodeSize;j++) {
					System.out.print(numberFormat.format(finalK[i][j])+ " ");
				}
				System.out.println();
			}
			System.out.println("Wektor F:");
			
			
			for(int i=0;i<nodeSize;i++) {
				System.out.println(finalF[i]);
			}
			
			System.out.println("Temperatury:");
			
			results = lsolve(finalK, finalF);//obliczanie temperatur
			for (int i = 0; i < nodeSize; i++) {
	            results[i]*=-1;//nie wiem czemu ale wyniki zawsze maj� warto�ci przeciwne, wi�c po prostu usuwam minusy
	        }
			for (int i = 0; i < nodeSize; i++) {
				network.nodes[i].t = results[i];
	            System.out.println(results[i]);
	        }
			
			for(int i=0;i<nodeSize;i++) { // zerowanie macierzy globalnych
				finalF[i] = 0.0;
				for(int j = 0;j<nodeSize;j++) {
					finalK[i][j] = 0.0;
				}
			}
			
			for(int i=0;i<elementSize;i++) {  //zerowanie macierzy lokalnych
				network.elements[i].F0();
			}
			
			//Zapisywanie wynik�w do pliku
				
		}
		try {
			FileWriter file = new FileWriter("files/results.txt");
			PrintWriter out = new PrintWriter(file);
			String all ="";
			for (int i = 0; i < nodeSize; i++) {
			all+="t["+i+"] = "+results[i]+ "\n";
			}
			out.write( all);
			out.close();
		}catch(Exception e) {System.out.println("Unlucky zapisywanie");}
	}
}